import React from 'react';
import './App.css';
import CountryList from './components/CountryList';

function App() {
  return (
      <CountryList/>
  );
}

export default App;
